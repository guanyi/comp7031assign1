package ca.guanyi.comp7031assign1;

/**
 * Created by Guanyi on 9/19/2016.
 */
public class CircleParameter {
    private int circleCenterX;
    private int circleCenterY;
    private int cirCleRadius = 100;

    public CircleParameter(int centerX, int centerY, int radius) {
        circleCenterX = centerX;
        circleCenterY = centerY;
        cirCleRadius = radius;
    }

    public int getCircleCenterX() { return circleCenterX; }
    public int getCircleCenterY() { return circleCenterY; }
    public int getCircleRadius() { return cirCleRadius; }

    public void setCircleCenterX(int centerX) {
        circleCenterX = centerX;
    }

    public void setCircleCenterY(int centerY) {
        circleCenterY = centerY;
    }

    public void setCircleRadius(int radius) {
        cirCleRadius = radius;
    }
}
