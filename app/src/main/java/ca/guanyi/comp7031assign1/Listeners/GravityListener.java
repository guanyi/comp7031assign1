package ca.guanyi.comp7031assign1.Listeners;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Guanyi on 10/5/2016.
 */

public class GravityListener implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor gravitySensor;
    private GravitySensible obj;

    public GravityListener(Context context, GravitySensible obj) {
        sensorManager = (SensorManager)context.getSystemService(context.getApplicationContext().SENSOR_SERVICE);
        gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        this.obj = obj;
    }

    public void register() {
        sensorManager.registerListener(this, gravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregister() {
        sensorManager.unregisterListener(this, gravitySensor);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        obj.onSensorChanged(sensorEvent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        obj.onAccuracyChanged(sensor, i);
    }
}
