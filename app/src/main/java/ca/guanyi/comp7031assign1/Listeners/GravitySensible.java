package ca.guanyi.comp7031assign1.Listeners;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by Guanyi on 10/5/2016.
 */

public interface GravitySensible {
    void onSensorChanged(SensorEvent sensorEvent);
    void onAccuracyChanged(Sensor sensor, int i);
}
