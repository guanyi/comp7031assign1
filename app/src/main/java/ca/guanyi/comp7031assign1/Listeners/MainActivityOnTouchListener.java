package ca.guanyi.comp7031assign1.Listeners;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import ca.guanyi.comp7031assign1.PaintingPlace;
import ca.guanyi.comp7031assign1.R;

/**
 * Created by Guanyi on 9/30/2016.
 */

public class MainActivityOnTouchListener implements OnTouchListener {

    private PaintingPlace paintingPlace;
    private int MOVE_DISTANCE = 0;

    public MainActivityOnTouchListener(PaintingPlace paintingPlace, int moveDistance) {
        this.paintingPlace = paintingPlace;
        MOVE_DISTANCE = moveDistance;
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.btnUp :
                paintingPlace.moveUpCircle(MOVE_DISTANCE);
                break;
            case R.id.btnDown :
                paintingPlace.moveDownCircle(MOVE_DISTANCE);
                break;
            case R.id.btnLeft :
                paintingPlace.moveLeftCircle(MOVE_DISTANCE);
                break;
            case R.id.btnRight :
                paintingPlace.moveRightCircle(MOVE_DISTANCE);
                break;
            default:
        }
        return true;
    }
}
