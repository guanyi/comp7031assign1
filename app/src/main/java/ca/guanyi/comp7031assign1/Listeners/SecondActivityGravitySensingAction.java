package ca.guanyi.comp7031assign1.Listeners;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.util.Log;
import ca.guanyi.comp7031assign1.PaintingPlace;

/**
 * Created by Guanyi on 10/5/2016.
 */

public class SecondActivityGravitySensingAction implements GravitySensible {
    private PaintingPlace paintingPlace;
    private int MOVE_DISTANCE = 0;

    public SecondActivityGravitySensingAction(PaintingPlace paintingPlace, int moveDistance) {
        this.paintingPlace = paintingPlace;
        MOVE_DISTANCE = moveDistance;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        //range from [-9.9 to 9.9]
        //[0] left higher number is negative, right higher is positive.
        //[1] top higher is positive, bottom higher is negative
        //[2] screen side higher is positive, bottom side higher is negative, face down is -9.9

        float[] values = sensorEvent.values;
        for(int i = 0; i < values.length; i++) {
            Log.i("sensor-"+i, String.valueOf(values[i]));
        }
        Log.i("sensor-", "-----------------");

        if (sensorEvent.values[0] < -1)
            paintingPlace.moveRightCircle(MOVE_DISTANCE);
        if (sensorEvent.values[0] > 1)
            paintingPlace.moveLeftCircle(MOVE_DISTANCE);
        if (sensorEvent.values[1] < -1)
            paintingPlace.moveUpCircle(MOVE_DISTANCE);
        if (sensorEvent.values[1] > 1)
            paintingPlace.moveDownCircle(MOVE_DISTANCE);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) { }
}
