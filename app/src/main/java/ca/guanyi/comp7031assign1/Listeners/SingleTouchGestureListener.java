package ca.guanyi.comp7031assign1.Listeners;

import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;

import java.util.ArrayList;

import ca.guanyi.comp7031assign1.MainActivity;
import ca.guanyi.comp7031assign1.PaintingPlace;
import ca.guanyi.comp7031assign1.R;

/**
 * Created by Guanyi on 9/30/2016.
 */

public class SingleTouchGestureListener implements GestureOverlayView.OnGesturePerformedListener {

    private GestureLibrary gestureLib;
    private PaintingPlace paintingPlace;
    private Context context;
    private int MOVE_DISTANCE = 0;

    public SingleTouchGestureListener(Context context, GestureLibrary gestureLib, PaintingPlace paintingPlace) {
        this.gestureLib = gestureLib;
        this.paintingPlace = paintingPlace;
        this.context = context;
        this.MOVE_DISTANCE = context.getResources().getInteger(R.integer.MOVE_DISTANCE);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {
        ArrayList<Prediction> predictions = gestureLib.recognize(gesture);
        if (predictions.size() > 0) {
            if (predictions.get(0).score > 1.0) {
                String action = predictions.get(0).name;

                if ("left".equals(action)) {
                    paintingPlace.moveLeftCircle(MOVE_DISTANCE);
                } else if ("right".equals(action)) {
                    paintingPlace.moveRightCircle(MOVE_DISTANCE);
                } else if ("up".equals(action)) {
                    paintingPlace.moveUpCircle(MOVE_DISTANCE);
                } else if ("down".equals(action)) {
                    paintingPlace.moveDownCircle(MOVE_DISTANCE);
                } else if ("check".equals(action)) {
                    ((MainActivity)context).launchSecondActivity();
                } else if ("circle".equals(action)) {
                    ((MainActivity)context).finish();
                } else {}
            }
        }
    }
}
