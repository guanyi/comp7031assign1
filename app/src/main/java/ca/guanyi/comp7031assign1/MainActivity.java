package ca.guanyi.comp7031assign1;

import android.app.Activity;
import android.content.Intent;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import ca.guanyi.comp7031assign1.Listeners.MainActivityOnTouchListener;
import ca.guanyi.comp7031assign1.Listeners.SingleTouchGestureListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private PaintingPlace paintingPlace;
    private int MOVE_DISTANCE = 0;
    private int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private Button btnUp, btnDown, btnLeft, btnRight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initVariables();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }



    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            paintingPlace.setCircleParameter(new CircleParameter(data.getIntExtra("circleCenterX", 0),
                    data.getIntExtra("circleCenterY", 0),
                    data.getIntExtra("circleRadius", 0)));
        }
    }

    public void btnLaunchClicked(View view) {
        launchSecondActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLeft :
                paintingPlace.moveLeftCircle(MOVE_DISTANCE);
                break;
            case R.id.btnRight :
                paintingPlace.moveRightCircle(MOVE_DISTANCE);
                break;
            default:
        }
    }

    public void launchSecondActivity() {
        Intent i = new Intent(this, SecondActivity.class);
        i.putExtra("circleCenterX", paintingPlace.getCircleParameter().getCircleCenterX());
        i.putExtra("circleCenterY", paintingPlace.getCircleParameter().getCircleCenterY());
        i.putExtra("circleRadius", paintingPlace.getCircleParameter().getCircleRadius());
        startActivityForResult(i, SECOND_ACTIVITY_REQUEST_CODE);
    }

    private void initVariables() {
        btnUp = (Button)findViewById(R.id.btnUp);
        btnDown = (Button)findViewById(R.id.btnDown);
        btnLeft = (Button)findViewById(R.id.btnLeft);
        btnRight = (Button)findViewById(R.id.btnRight);

        MOVE_DISTANCE = getResources().getInteger(R.integer.MOVE_DISTANCE);
        SECOND_ACTIVITY_REQUEST_CODE = getResources().getInteger(R.integer.SECOND_ACTIVITY_REQUEST_CODE);
        paintingPlace = new PaintingPlace(this);
        GestureOverlayView gestureFrameLayoutMain = (GestureOverlayView)findViewById(R.id.gestureFrameLayoutMain);
        gestureFrameLayoutMain.addView(paintingPlace);
        loadGestureListeners(gestureFrameLayoutMain);

        btnLeft.setOnClickListener(this);
        btnRight.setOnClickListener(this);
        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                paintingPlace.moveUpCircle(MOVE_DISTANCE);
            }
        });

        btnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                paintingPlace.moveDownCircle(MOVE_DISTANCE);
            }
        });

        MainActivityOnTouchListener mainActivityOnTouchListener = new MainActivityOnTouchListener(paintingPlace, MOVE_DISTANCE);
        btnUp.setOnTouchListener(mainActivityOnTouchListener);
        btnDown.setOnTouchListener(mainActivityOnTouchListener);
        btnLeft.setOnTouchListener(mainActivityOnTouchListener);
        btnRight.setOnTouchListener(mainActivityOnTouchListener);
    }

    private void loadGestureListeners(GestureOverlayView gestureFrameLayoutMain) {
        GestureLibrary gestureLib = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if (!gestureLib.load()) {
            finish();
        }
        else {
            gestureFrameLayoutMain.addOnGesturePerformedListener(new SingleTouchGestureListener(this, gestureLib, paintingPlace));
        }
    }
}
