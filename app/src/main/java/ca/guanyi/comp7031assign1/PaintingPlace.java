package ca.guanyi.comp7031assign1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Guanyi on 9/19/2016.
 */
public class PaintingPlace extends View {

    private CircleParameter circleParameter;
    private int paintPlaceWidth;
    private int paintPlaceHeight;

    public PaintingPlace(Context context) {
        super(context);
    }

    public PaintingPlace(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paintPlaceWidth = canvas.getWidth();
        paintPlaceHeight = canvas.getHeight();

        Paint paint = new Paint();
        paint.setColor(Color.GRAY);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);

        paint.setColor(Color.YELLOW);
        paint.setTextSize(48f);
        canvas.drawText("Use gestures on the gray area to move the circle", 40, 60, paint);

        if (circleParameter == null)  {
            circleParameter = new CircleParameter(paintPlaceWidth / 2, paintPlaceHeight / 2, 100);
        }
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(circleParameter.getCircleCenterX(),
                          circleParameter.getCircleCenterY(),
                          circleParameter.getCircleRadius(),
                          paint);
    }

    public void setCircleParameter(CircleParameter updatedCircleParameter) {
        circleParameter = updatedCircleParameter;
    }

    public CircleParameter getCircleParameter() {
        return circleParameter;
    }

    public void moveUpCircle(int upDistance) {
        if (circleParameter.getCircleCenterY() > circleParameter.getCircleRadius()) {
            circleParameter.setCircleCenterY(circleParameter.getCircleCenterY() - upDistance);
            this.invalidate();
        }
    }

    public void moveDownCircle(int downDistance) {
        if (paintPlaceHeight - circleParameter.getCircleCenterY() > circleParameter.getCircleRadius()) {
            circleParameter.setCircleCenterY(circleParameter.getCircleCenterY() + downDistance);
            this.invalidate();
        }
    }

    public void moveLeftCircle(int leftDistance) {
        if (circleParameter.getCircleCenterX() > circleParameter.getCircleRadius()) {
            circleParameter.setCircleCenterX(circleParameter.getCircleCenterX() - leftDistance);
            this.invalidate();
        }
    }

    public void moveRightCircle(int rightDistance) {
        if (paintPlaceWidth - circleParameter.getCircleCenterX() > circleParameter.getCircleRadius()) {
            circleParameter.setCircleCenterX(circleParameter.getCircleCenterX() + rightDistance);
            this.invalidate();
        }
    }

}
