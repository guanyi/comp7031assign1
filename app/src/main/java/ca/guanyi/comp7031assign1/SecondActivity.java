package ca.guanyi.comp7031assign1;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import ca.guanyi.comp7031assign1.Listeners.GravityListener;
import ca.guanyi.comp7031assign1.Listeners.SecondActivityGravitySensingAction;

public class SecondActivity extends AppCompatActivity
                            implements GestureDetector.OnDoubleTapListener,
                                       GestureDetector.OnGestureListener {

    private PaintingPlace paintingPlace;
    private int MOVE_DISTANCE = 0;
    private GestureDetectorCompat gestureDetector;

    //GravityListener is a helper class I created for this project, and it is not from Android Framework.
    //GravityListener provides a very general way to initialize related variables and a structure of
    //necessary override methods. By using this helper class, the SecondaryActivity doesn't need to
    //know the detail of how to create and use the listener.
    //The SecondaryActivity only need to do 2 things to use it.
    //1, uses the API provided by "GravityListener" to create, register or unregister the gravity listener.
    //2, create a separate "SecondActivityGravitySensingAction" class which includes the variables that will
    //change during the gravity sensing change and the methods of how to handle the change.
    //The advantage of this kind of implementation is to separate lots of code from SecondaryActivity class,
    //and gravity initialization code can be reused if more than one gravity listeners in this project
    //The disadvantage is that it makes related information separate into many classes or interfaces, so
    //we need to understand the relation between these classes and interfaces to understand the code.
    private GravityListener gravityListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent i = getIntent();
        paintingPlace = new PaintingPlace(this);
        paintingPlace.setCircleParameter(new CircleParameter(i.getIntExtra("circleCenterX", 0),
                                                             i.getIntExtra("circleCenterY", 0),
                                                             i.getIntExtra("circleRadius", 0)));

        FrameLayout frameLayoutSecond = (FrameLayout)findViewById(R.id.frameLayoutSecond);
        frameLayoutSecond.addView(paintingPlace);

        MOVE_DISTANCE = getResources().getInteger(R.integer.MOVE_DISTANCE);

        //Questions: when is the best to create, set and unset these listeners?
        gestureDetector = new GestureDetectorCompat(this, this);
        gestureDetector.setOnDoubleTapListener(this);
        gravityListener = new GravityListener(this, new SecondActivityGravitySensingAction(paintingPlace, MOVE_DISTANCE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        gravityListener.register();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //gestureDetector.setOnDoubleTapListener(null);
        gravityListener.unregister();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        Intent backToMainActivity = new Intent(this, MainActivity.class);
        backToMainActivity.putExtra("circleCenterX", paintingPlace.getCircleParameter().getCircleCenterX());
        backToMainActivity.putExtra("circleCenterY", paintingPlace.getCircleParameter().getCircleCenterY());
        backToMainActivity.putExtra("circleRadius", paintingPlace.getCircleParameter().getCircleRadius());

        setResult(Activity.RESULT_OK, backToMainActivity);
        finish();
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }
}
